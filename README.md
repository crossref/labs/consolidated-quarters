# Consolidated Quarters

## What is it?

A simple UI to allow Crossref support (or anybody else) to select and download a CSV file of the quarterly registration reports for a sponsor to get an estimate of what they will be billed for.

## How does it work?

The Labs application, "billy-bob" pre-generates quarterly billy reports that are, in turn, included in [Crossref Labs Reports](https://prep.crossref.org). This app just consolidates all the reports from billy-bob and makes the resulting file a filterable Pandas DataFrame. The filtered data can, in turn, be downloaded as a CSV file.

- `utils/consolidate-quarters.py` consolidates the data. The resulting files should be put in the `data/` directory.
- `utils/create-sponsoring-member-map.py` uses data gathered from the Labs API `/members` route to create a mapping of sponsoring members to accounting-ids.  
- `streamlit run app.py` creates the UI that allows you to navigate the data.

## Can I see it running?

<https://consolidated-quarters.fly.dev>
