import streamlit as st
from cq.sessions import new_session, params_to_state, state_to_params
from cq.ui import display_main
from cq.data import load_favicon

st.set_page_config(
    layout="wide",
    page_title="Crossref Consolidated Quarterlies",
    page_icon=load_favicon(),
)
st.image("https://assets.crossref.org/logo/labs/labs-logo-ribbon.svg", width=200)
st.title("Consolidated Quarterlies")

if "inited" not in st.session_state:
    # This is the first time the app is run
    # set everything to the default state
    # and then override defaults with any
    # **allowed* params in the URL
    new_session()
    params_to_state()

# If we are inited *or* this is a continued session,
# then we want to copy the **allowed** params to the URL
state_to_params()

# And then we go about our main business
display_main()
