import logging
import typer
from pathlib import Path
import json
from collections import Counter


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
logger.info("Starting program")


def main(member_file: Path = typer.Argument(..., exists=True)):
    logger.info(f"member_file: {member_file}")
    with open(member_file, "r") as f:
        members = json.load(f)

    account_types = Counter()
    sponsors = Counter()
    for member in members["message"]["items"]:
        account_type = member.get("cr-labs-member-profile", {}).get(
            "account-type", "unknown"
        )
        account_types[account_type] += 1
        sponser_name = member.get("cr-labs-member-profile", {}).get(
            "sponsor-name", None
        )
        sponsors[sponser_name] += 1

    print(json.dumps(account_types, indent=2))
    print("**********")
    print(json.dumps(sponsors.most_common(5), indent=2))
    logger.info("Ending program")


if __name__ == "__main__":
    typer.run(main)
