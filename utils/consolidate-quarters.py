# Create a giant aggregation of all the quarters.

# get the paths to all the "all.parquet" files in billing_dir or its subdirectories
import logging
import typer
from pathlib import Path
import pandas
import glob


def all_billing_paths(billing_dir):
    return glob.glob(f"{billing_dir}/**/all.parquet", recursive=True)


def extract_period(billing_path):
    period = Path(billing_path).parent.name.split("-")[0]
    print(period)
    return period


def split_period(period):
    # first for characters are the year
    year = period[:4]
    # last 2 characters are the quarter
    quarter = period[-2:]
    return year, quarter


def add_period(df, period):
    year, quarter = split_period(period)
    df["year"] = year
    df["quarter"] = quarter

    return df


def main(
    billing_dir: Path = typer.Option(..., help="Path to the billing directory"),
    output_file: Path = typer.Option(..., help="Path to the output file"),
):
    logging.basicConfig(level=logging.INFO)
    billing_paths = all_billing_paths(billing_dir)
    logging.info(f"Found {len(billing_paths)} billing files")
    # billing_paths = billing_paths[:100]
    logging.info(f"Using {len(billing_paths)} billing files")

    billing_df = pandas.concat(
        [
            add_period(pandas.read_parquet(billing_path), extract_period(billing_path))
            for billing_path in billing_paths
        ]
    )
    billing_df.to_parquet(output_file)
    logging.info(f"Wrote {len(billing_df)} rows to {output_file}")


if __name__ == "__main__":
    typer.run(main)
