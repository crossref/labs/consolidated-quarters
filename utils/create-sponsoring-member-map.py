import logging
import typer
from pathlib import Path
import json
from json import JSONEncoder
from collections import Counter, defaultdict


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
logger.info("Starting program")

name_to_account = defaultdict(set)
account_to_name = defaultdict(set)


# subclass JSONEncoder to handle sets
class setEncoder(JSONEncoder):
    def default(self, obj):
        return list(obj)


def main(member_file: Path = typer.Argument(..., exists=True)):
    logger.info(f"member_file: {member_file}")
    with open(member_file, "r") as f:
        members = json.load(f)

    for member in members["message"]["items"]:
        sponsor_name = member.get("cr-labs-member-profile", {}).get(
            "sponsor-name", None
        )
        if sponsor_name:
            accounting_id = member.get("cr-labs-member-profile", {}).get(
                "accounting-id", "unknown"
            )

            name_to_account[sponsor_name].add(accounting_id)
            account_to_name[accounting_id].add(sponsor_name)

    with open("data/name_to_account.json", "w") as f:
        json.dump(name_to_account, f, indent=2, cls=setEncoder)
    with open("data/account_to_name.json", "w") as f:
        json.dump(account_to_name, f, indent=2, cls=setEncoder)
    logger.info("done")


if __name__ == "__main__":
    typer.run(main)
