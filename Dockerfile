# syntax=docker/dockerfile:1
FROM python:3.11-slim
RUN apt-get update; apt-get install -y curl
# unzip gcc python3-dev
WORKDIR /code
# set virtual env
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
# RUN od -An -N1 -i /dev/random > random_num

COPY requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
# RUN ls
COPY . .
EXPOSE 5000


# NB turn off file watcher for streamlit because iwatch doesn't like working with so many
# files in the data directory in the container
CMD ["streamlit", "run","app.py"]
