import json
import pandas as pd
import streamlit as st
from PIL import Image


def load_favicon():
    return Image.open("static/favicon.ico")


def lowercase_columns(df):
    df.columns = [c.lower() for c in df.columns]
    return df


def cleanup_columns(df):
    df["year"] = df["year"].astype(str).str.pad(4, fillchar="0")
    df["year"] = df["year"].astype("category")
    df["quarter"] = df["quarter"].astype("category")
    # rename customer-id column to accounting-id
    df = df.rename(columns={"customer-id": "accounting-id"})
    df["accounting-id"] = df["accounting-id"].astype("category")

    df["user-id"].apply(
        lambda x: f"https://doi.crossref.org/servlet/depositUserAdmin?sf=search&userID={x}"
    )

    df = lowercase_columns(df)

    return df


@st.cache_data
def load_sponsors():
    with open("data/name_to_account.json") as f:
        return json.load(f)


def sponsor_names_to_accounts(name):
    return [] if name not in load_sponsors() else load_sponsors()[name]


@st.cache_data
def sponsor_names():
    return sorted(load_sponsors().keys())


@st.cache_data
def load_data():
    """Load data from parquet file"""
    df = cleanup_columns(pd.read_parquet("data/consolidated.parquet"))
    return df
