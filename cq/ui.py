import streamlit as st
from streamlit_extras.dataframe_explorer import dataframe_explorer

from cq.data import load_data, sponsor_names, sponsor_names_to_accounts

COLUMN_DISPLAY_NAMES = [
    "type",
    "title",
    "prefix",
    "description",
    "user-id",
    "accounting-id",
    "year",
    "quarter",
    "# cy",
    "# by",
]

LINK_FORMAT = {
    "user-id": st.column_config.LinkColumn(
        None,
        help="Login to the Crossref admin console to view user info",
        max_chars=100,
        display_text="https://doi\.crossref\.org/servlet/depositUserAdmin\?sf=search&userID=(.*)",
    )
}

# https://doi.crossref.org/servlet/publisherUserAdmin?sf=search&publisherID=101902
# https://doi.crossref.org/servlet/publisherUserAdmin?sf=search&publisherID=101902
# https://doi.crossref.org/servlet/depositUserAdmin?sf=search&userID=101903


def link_user_id(df):
    df["user-info"] = df["user-id"].apply(
        lambda x: f"https://doi.crossref.org/servlet/depositUserAdmin?sf=search&userID={x}"
    )
    df = df.drop(columns=["user-id"])
    return df


def display_accounting_ids():
    account_ids = sponsor_names_to_accounts(st.session_state.selected_sponsor)

    st.markdown(f'Sponsor\'s accounting IDs: `{", ".join(account_ids)}`')


def display_main():
    st.selectbox(
        "Sponsor",
        sponsor_names(),
        index=None,
        key="selected_sponsor",
        placeholder="Choose a sponsor",
    )

    display_accounting_ids()

    df = load_data()

    filtered_df = dataframe_explorer(df[COLUMN_DISPLAY_NAMES], case=False)

    sponsor_ids = sponsor_names_to_accounts(st.session_state.selected_sponsor)

    if len(sponsor_ids) > 0:
        filtered_df = filtered_df[filtered_df["accounting-id"].isin(sponsor_ids)]

    st.dataframe(filtered_df, column_config=LINK_FORMAT, use_container_width=True)

    st.markdown(f"**Current Year:** {filtered_df['# cy'].sum():,}")
    st.markdown(f"**Backfile:** {filtered_df['# by'].sum():,}")
    # download as csv
    st.download_button(
        label="Download data as CSV",
        data=filtered_df.to_csv(index=False),
        file_name=f"{st.session_state.selected_sponsor}.csv",
        mime="text/csv",
    )
