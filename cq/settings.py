APP_NAME = "Consolidated Quarters"

ABOUT = """
Try to provide a simple interface to the consolidated quarters data.

"""

DEFAULT_STATE = {
    "inited": False,
    "selected_sponsor": None,
    "quarter": None,
    "year": [],
}

ALLOWED_PARAMS = ["selected_sponsor"]
