import streamlit as st
from cq.settings import DEFAULT_STATE, ALLOWED_PARAMS


def cast_param_value(param, value):
    """Cast param value to correct type"""
    # Streamlit treats all params as lists, even if they are single values
    # So we need to convert them back to single values when appropriate
    # we use the values in DEFAULT_SHARED_STATE to determine if a param
    # should be a list or a single value
    truthy_values = {"true", "1", "t", "y", "yes", "yeah", "yup", "certainly", "uh-huh"}

    if isinstance(DEFAULT_STATE[param], list):
        return value
    if isinstance(DEFAULT_STATE[param], bool):
        return value[0].lower() in truthy_values

    return f"{value[0]}"


def params_to_state():
    for k in st.query_params:
        if k in ALLOWED_PARAMS:
            st.session_state[k] = cast_param_value(k, st.query_params.get_all(k))


def state_to_params():
    """Copy session state to URL"""
    for k, v in st.session_state.items():
        if k in ALLOWED_PARAMS:
            st.query_params[k] = v


def new_session():
    """Initialize session state"""
    st.session_state.update(DEFAULT_STATE)
